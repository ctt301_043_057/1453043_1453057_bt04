#include "Libraries\Headers\opencv2\core\core.hpp"
#include "Libraries\Headers\opencv2\highgui\highgui.hpp"
#include "Libraries\Headers\opencv2\imgproc\imgproc.hpp"

#include <iostream>
#include <algorithm>

using namespace cv;
using namespace std;

int robertFilter(Mat sourceImage, Mat destinationImage);
int sobelFilter(Mat sourceImage, Mat destinationImage);
int laplacianFilter(Mat sourceImage, Mat destinationImage);
int unsharpMaskingProc(Mat sourceImage, Mat	destinationImage, int k);
int replicating(Mat sourceImage, Mat& paddingImage, int w, int h);
void rgbToGray(Mat sourceImage, Mat destinationImage);
bool isGrayscale(Mat sourceImage);

int main(int argc, char** argv)
{
	// Check number of arguments and argv[2]
	if (argc > 3)
	{
		cout << " Nguyen Huong Thao  - 1453043" << endl;
		cout << " Nguyen Trong Tuyen - 1453057" << endl;
		return -1;
	}
	Mat sourceImage;

	// Use flag ANY_COLOR to read any color
	sourceImage = imread(argv[1], IMREAD_ANYCOLOR);
	// Check soure image
	if (!sourceImage.data)
	{
		cout << "Cannot open image" << std::endl;
		return -1;
	}

	Mat destinationImage = Mat::zeros(sourceImage.size(), sourceImage.type()); // Create destination image with the same size, type as source image to store image after being processed

	namedWindow("Original Image", WINDOW_AUTOSIZE);			// Window of source image
	namedWindow("Filtered Image", WINDOW_AUTOSIZE);			// Window of destination image

															// Cast command line parameter to int 
	int iCmd = atoi(argv[2]);
	switch (iCmd)
	{
	case 1: // robert cross gradient filter
		robertFilter(sourceImage, destinationImage);
		imshow("Original Image", sourceImage);
		imshow("Filtered Image", destinationImage);
		break;
	case 2:
		sobelFilter(sourceImage, destinationImage);
		imshow("Original Image", sourceImage);
		imshow("Filtered Image", destinationImage);
		break;
	case 3: //laplacian
		laplacianFilter(sourceImage, destinationImage);
		imshow("Original Image", sourceImage);
		imshow("Filtered Image", destinationImage);
		break;
	case 4:
		break;
	default:
		break;
	}
	waitKey(0);
	return 0;
}

int robertFilter(Mat sourceImage, Mat destinationImage)
{
	// Create destination image with the same size, type as source image to store image after being processed
	//Mat tempImage = Mat::zeros(sourceImage.size(), sourceImage.type()); 
	Mat tempImage;
	replicating(sourceImage, tempImage, 3, 3);
	//imshow("Test Image", tempImage);
	//waitKey(0);
	for (int row = 3 / 2; row < tempImage.rows - 3 / 2; row++)
	{
		for (int col = 3 / 2; col < tempImage.cols - 3 / 2; col++)
		{
			if ((sourceImage.type() == CV_8U || sourceImage.type() == CV_8S))
			{
				// evaluate mask = M(x,y) = | z9 - z5 | + |z8 - z6|
				int iMask = abs(tempImage.at<uchar>(row + 1, col + 1) - tempImage.at<uchar>(row, col)) + abs(tempImage.at<uchar>(row + 1, col) - tempImage.at<uchar>(row, col + 1));
				destinationImage.at<uchar>(row - 3 / 2, col - 3 / 2) = (uchar)(iMask);
			}
			else
			{
				for (int channel = 0; channel < tempImage.channels(); channel++)
				{
					// evaluate mask = M(x,y) = | z9 - z5 | + |z8 - z6|
					int iMask = abs(tempImage.at<Vec3b>(row + 1, col + 1)[channel] - tempImage.at<Vec3b>(row, col)[channel]) + abs(tempImage.at<Vec3b>(row + 1, col)[channel] - tempImage.at<Vec3b>(row, col + 1)[channel]);
					destinationImage.at<Vec3b>(row - 3 / 2, col - 3 / 2)[channel] = (uchar)(iMask);
				}
			}
		}
	}
	return 0;
}

int replicating(Mat sourceImage, Mat& paddingImage, int w, int h)
{
	// Create paddingImgae with rows = rows + h-1, cols = cols + w-1
	// create() automatically sets 0 to all values of paddingImage
	paddingImage.create(sourceImage.rows + h - 1, sourceImage.cols + w - 1, sourceImage.type());
	// Copy values of sourceImage to paddingImage
	for (int row = h / 2; row < paddingImage.rows - h / 2; row++)
	{
		for (int col = w / 2; col < paddingImage.cols - w / 2; col++)
		{
			if (sourceImage.type() == CV_8U || sourceImage.type() == CV_8S)
			{
				paddingImage.at<uchar>(row, col) = sourceImage.at<uchar>(row - h / 2, col - w / 2);
			}
			else
			{
				for (int channel = 0; channel < paddingImage.channels(); channel++)
				{
					// matrix sourceImage starts from row - h /2, col - w /2
					paddingImage.at<Vec3b>(row, col)[channel] = sourceImage.at<Vec3b>(row - h / 2, col - w / 2)[channel];
				}
			}
		}
	}
	// Replicate left of the image
	for (int row = h / 2; row < paddingImage.rows - h / 2; row++)
	{
		if (sourceImage.type() == CV_8U || sourceImage.type() == CV_8S)
		{
			for (int col = w / 2 - 1; col >= 0; col--)
			{
				paddingImage.at<uchar>(row, col) = paddingImage.at<uchar>(row, col + 1);
			}

		}
		else
		{
			for (int channel = 0; channel < paddingImage.channels(); channel++)
			{
				for (int col = w / 2 - 1; col >= 0; col--)
				{
					paddingImage.at<Vec3b>(row, col)[channel] = paddingImage.at<Vec3b>(row, col + 1)[channel];
				}
			}
		}
	}
	// Replicate right of the image
	for (int row = h / 2; row < paddingImage.rows - h / 2; row++)
	{
		if (sourceImage.type() == CV_8U || sourceImage.type() == CV_8S)
		{
			for (int col = paddingImage.cols - w / 2; col < paddingImage.cols; col++)
			{
				paddingImage.at<uchar>(row, col) = paddingImage.at<uchar>(row, col - 1);
			}
		}
		else
		{
			for (int channel = 0; channel < paddingImage.channels(); channel++)
			{
				for (int col = paddingImage.cols - w / 2; col < paddingImage.cols; col++)
				{
					paddingImage.at<Vec3b>(row, col)[channel] = paddingImage.at<Vec3b>(row, col - 1)[channel];
				}
			}
		}
	}
	// Replicate top of the image
	for (int col = 0; col < paddingImage.cols; col++)
	{
		if (sourceImage.type() == CV_8U || sourceImage.type() == CV_8S)
		{
			for (int row = h / 2 - 1; row >= 0; row--)
			{
				paddingImage.at<uchar>(row, col) = paddingImage.at<uchar>(row + 1, col);
			}
		}
		else
		{
			for (int channel = 0; channel < paddingImage.channels(); channel++)
			{
				for (int row = h / 2 - 1; row >= 0; row--)
				{
					paddingImage.at<Vec3b>(row, col)[channel] = paddingImage.at<Vec3b>(row + 1, col)[channel];
				}
			}
		}
	}
	// Replicate bottom of the image
	for (int col = 0; col < paddingImage.cols; col++)
	{
		if (sourceImage.type() == CV_8U || sourceImage.type() == CV_8S)
		{
			for (int row = paddingImage.rows - h / 2; row < paddingImage.rows; row++)
			{
				paddingImage.at<uchar>(row, col) = paddingImage.at<uchar>(row - 1, col);
			}
		}
		else
		{
			for (int channel = 0; channel < paddingImage.channels(); channel++)
			{
				for (int row = paddingImage.rows - h / 2; row < paddingImage.rows; row++)
				{
					paddingImage.at<Vec3b>(row, col)[channel] = paddingImage.at<Vec3b>(row - 1, col)[channel];
				}
			}
		}
	}
	return 0;
}

int laplacianFilter(Mat sourceImage, Mat destinationImage)
{
	Mat paddingImg;
	Mat channel[3];

	if (sourceImage.type() == CV_8U || sourceImage.type() == CV_8S)
		replicating(sourceImage, paddingImg, 3, 3);
	else {
		//convert to YUV
		cvtColor(sourceImage, sourceImage, CV_BGR2YCrCb);
		split(sourceImage, channel);

		//channel 0: Y channel, only perform Laplacian filter on this channel
		replicating(channel[0], paddingImg, 3, 3);
	}


	Mat mask(sourceImage.rows, sourceImage.cols, DataType<float>::type);

	float minVal = 255, maxVal = 0;
	for (int row = 1; row <= sourceImage.rows; ++row)
	{
		for (int col = 1; col <= sourceImage.cols; ++col)
		{
			mask.at<float>(row - 1, col - 1) =
				-paddingImg.at<uchar>(row - 1, col - 1) - paddingImg.at<uchar>(row - 1, col) - paddingImg.at<uchar>(row - 1, col + 1)
				- paddingImg.at<uchar>(row, col - 1) + 8 * paddingImg.at<uchar>(row, col) - paddingImg.at<uchar>(row, col + 1)
				- paddingImg.at<uchar>(row + 1, col - 1) - paddingImg.at<uchar>(row + 1, col) - paddingImg.at<uchar>(row + 1, col + 1);
			minVal = min(mask.at<float>(row - 1, col - 1), minVal);
			maxVal = max(mask.at<float>(row - 1, col - 1), maxVal);
		}
	}
	//scale the filtered data to [0, 255] 
	float minSharpVal = 255, maxSharpVal = 0;
	for (int row = 0; row < sourceImage.rows; ++row)
	{
		for (int col = 0; col < sourceImage.cols; ++col)
		{
			float val = (mask.at<float>(row, col) - minVal) * (255.0 / maxVal);
			mask.at<float>(row, col) = val;
			minSharpVal = min(val, minSharpVal);
			maxSharpVal = max(val, maxSharpVal);
		}
	}
	//add original intensity value scale sharpened image to [0,255]
	if (sourceImage.type() == CV_8U || sourceImage.type() == CV_8S) {
		for (int row = 0; row < sourceImage.rows; ++row) {
			for (int col = 0; col < sourceImage.cols; ++col) {
				mask.at<float>(row, col) += sourceImage.at<uchar>(row, col);
				destinationImage.at<uchar>(row, col) = (mask.at<float>(row, col) - minSharpVal) * (255.0 / maxSharpVal);
			}
		}
	}
	else {
		for (int row = 0; row < sourceImage.rows; ++row) {
			for (int col = 0; col < sourceImage.cols; ++col) {
				mask.at<float>(row, col) += channel[0].at<uchar>(row, col);
				channel[0].at<uchar>(row, col) = (mask.at<float>(row, col) - minSharpVal) * (255.0 / maxSharpVal);
			}
		}
		merge(channel, 3, destinationImage);
		cvtColor(sourceImage, sourceImage, CV_YCrCb2BGR);
		cvtColor(destinationImage, destinationImage, CV_YCrCb2BGR);
	}

	return 0;
}

int sobelFilter(Mat sourceImage, Mat destinationImage)
{
	// Create destination image with the same size, type as source image to store image after being processed
	//Mat tempImage = Mat::zeros(sourceImage.size(), sourceImage.type()); 
	Mat tempImage;
	replicating(sourceImage, tempImage, 3, 3);
	//imshow("Test Image", tempImage);
	//waitKey(0);
	for (int row = 3 / 2; row < tempImage.rows - 3 / 2; row++)
	{
		for (int col = 3 / 2; col < tempImage.cols - 3 / 2; col++)
		{
			if ((sourceImage.type() == CV_8U || sourceImage.type() == CV_8S))
			{
				// evaluate mask = M(x,y) = | (z7 + 2z8 + z9) - (z1 + 2z7 + z3) | + | (z3 + 2z6 + z9) - (z1 + 2z4 + z7) |
				int iMask = abs((tempImage.at<uchar>(row + 1, col - 1) + 2 * tempImage.at<uchar>(row + 1, col) + tempImage.at<uchar>(row + 1, col + 1)) -
					(tempImage.at<uchar>(row - 1, col - 1) + 2 * tempImage.at<uchar>(row + 1, col - 1) + tempImage.at<uchar>(row - 1, col + 1)))
					+ abs((tempImage.at<uchar>(row - 1, col + 1) + 2 * tempImage.at<uchar>(row, col + 1) + tempImage.at<uchar>(row + 1, col + 1)) -
					(tempImage.at<uchar>(row - 1, col - 1) + 2 * tempImage.at<uchar>(row, col - 1) + tempImage.at<uchar>(row + 1, col - 1)));
				destinationImage.at<uchar>(row - 3 / 2, col - 3 / 2) = (uchar)(iMask);
			}
			else
			{
				for (int channel = 0; channel < tempImage.channels(); channel++)
				{
					// evaluate mask = M(x,y) = | (z7 + 2z8 + z9) - (z1 + 2z7 + z3) | + | (z3 + 2z6 + z9) - (z1 + 2z4 + z7) |
					int iMask = abs((tempImage.at<Vec3b>(row + 1, col - 1)[channel] + 2 * tempImage.at<Vec3b>(row + 1, col)[channel] + tempImage.at<Vec3b>(row + 1, col + 1)[channel]) -
						(tempImage.at<Vec3b>(row - 1, col - 1)[channel] + 2 * tempImage.at<Vec3b>(row + 1, col - 1)[channel] + tempImage.at<Vec3b>(row - 1, col + 1)[channel]))
						+ abs((tempImage.at<Vec3b>(row - 1, col + 1)[channel] + 2 * tempImage.at<Vec3b>(row, col + 1)[channel] + tempImage.at<Vec3b>(row + 1, col + 1)[channel]) -
						(tempImage.at<Vec3b>(row - 1, col - 1)[channel] + 2 * tempImage.at<Vec3b>(row, col - 1)[channel] + tempImage.at<Vec3b>(row + 1, col - 1)[channel]));
					destinationImage.at<Vec3b>(row - 3 / 2, col - 3 / 2)[channel] = (uchar)(iMask);
				}
			}
		}
	}
	return 0;
}

int unsharpMaskingProc(Mat sourceImage, Mat	destinationImage, int k)
{}

bool isGrayscale(Mat sourceImage)
{
	if (sourceImage.type() == CV_8UC1)
		return true;
	else
		return false;
}

void rgbToGray(Mat sourceImage, Mat destinationImage)
{
	// Check grayscale with CV_8UC1
	if (sourceImage.type() == CV_8UC1)
	{
		return;
	}
	// Traverse the matrix by rows, cols, channels
	for (int row = 0; row < sourceImage.rows; row++)
	{
		for (int col = 0; col < sourceImage.cols; col++)
		{
			// channels() return number of channels
			for (int channel = 0; channel < sourceImage.channels(); channel++)
			{
				// Compute average of 3 channels B, G, R
				// value = ( Blue + Green + Red ) / 3
				destinationImage.at<Vec3b>(row, col)[channel] = (sourceImage.at<Vec3b>(row, col)[0] + sourceImage.at<Vec3b>(row, col)[1] + sourceImage.at<Vec3b>(row, col)[2]) / 3;
			}
		}
	}
}